const mongoose  = require('mongoose');

const dbConnect = () => {
    const DB_URI = process.env.DB_URI
    mongoose.connect(DB_URI, {
        useNewUrlParser: true, 
        useUnifiedTopology: true
    }, (err,resp)=>{
        if(!err) console.log('*****      CONEXIÓN EXITOSA    ******');
        else
            console.log('*****      CONEXIÓN FALLIDA    ******');
    })
}

module.exports = {dbConnect}
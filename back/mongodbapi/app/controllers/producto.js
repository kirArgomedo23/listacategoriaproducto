
const httpError = require('../helpers/handleError');
const validarCrearProducto = require('../validaciones/producto');
const productoModel = require('../models/producto');
const categoriaModel = require('../models/categoria');

const crearProducto = async (req, res) => {

    const { error } = validarCrearProducto.validate(req.body);

    if (error) {
        return res.status(400).json({error: error.details[0].message})
    }
    
    
    try{
        const {nombre, descripcion,precioUnitario, cantidad, categoria} = req.body;
        const categoriaFind = await categoriaModel.findOne({nombre:categoria});
        
        if(categoriaFind == null) {
            res.status(400).send({'msg':'No existe la categoría. Por favor, intente con otra categoría.'});
        }else{
            const categoria = categoriaFind.id;
            const resDetail = await productoModel.create({
                nombre, descripcion,precioUnitario,cantidad, categoria
            })

            res.status(200).send({'msg':'Creado con éxito'});
        }
    }catch(e){
        httpError(res,e);
    }

}



const listarProductos = async (req, res) => {

    try{
        let listProductos = await productoModel.find({});
        listProductos = await categoriaModel.populate(listProductos, {path:"categoria"} );
        if(listProductos.length <= 0){
            res.status(400).send({'msg':'No se encontró datos'});
        }else{
            res.status(200).send({'msg':'Consulta ejecutada con éxito', 'data': listProductos});
        }
    }catch(e){
        httpError(res,e);
    }

}

module.exports = {crearProducto,listarProductos}
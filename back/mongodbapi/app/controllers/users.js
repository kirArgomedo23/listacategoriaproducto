
const httpError = require('../helpers/handleError');
const userModel = require('../models/users');
const {validarRegistroUsuario, validarLoginUsuario} = require('../validaciones/users');
const {generarToken} = require('../helpers/token');

const getItems = async (req, res) =>{
    try{
        const listAll = await userModel.find({});
        res.send({data:listAll});
    }catch(e){
        httpError(res,e);
    }
}


const getItem = async (req, res) => {
    try{
        let _id = req.params._id;
        console.log(_id);  
        const user = await userModel.findOne({_id});
        res.send({data:user});
    }catch(e){
        httpError(res,e);
    }
}

const createItem = async (req, res) => {

    const { error } = validarRegistroUsuario.validate(req.body);

    if (error) {
        console.log('error valid: ', error)
        return res.status(400).json({error: error.details[0].message})
    }
    try{
        const {nombre, apellidoPaterno,apellidoMaterno, email, fechaNacimiento, alias, contrasenia} = req.body;
        const resDetail = await userModel.create({
            nombre, apellidoPaterno,apellidoMaterno, email, fechaNacimiento, alias, contrasenia
        })
    
        res.send({data: resDetail});
        
    }catch(e){
        console.log('error')
        httpError(res,e);
    }
}

const loginUsuario = async (req, res) => {
    const { error } = validarLoginUsuario.validate(req.body);

    if (error) {
        console.log('error valid: ', error)
        return res.status(400).json({error: error.details[0].message})
    }

    const {alias, contrasenia} = req.body;


    const user = await userModel.findOne({alias, contrasenia});
    if(!user)
        res.status(400).json({error:'Credenciales incorrectas.'})
    
    let token = generarToken(req);

    res.status(200).json({msg:'Credenciales correctas', token})
}



module.exports = {getItems,getItem,createItem,loginUsuario}
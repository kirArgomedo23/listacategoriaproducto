
const httpError = require('../helpers/handleError');
const validarCrearCategoria = require('../validaciones/categoria');
const categoriaModel = require('../models/categoria')

const crearCategoria = async (req, res) => {
    
    const { error } = validarCrearCategoria.validate(req.body);

    if (error) {
        console.log('error valid: ', error)
        return res.status(400).json({error: error.details[0].message})
    }
    
    try{
        const {nombre, tipo,descripcion} = req.body;
        const resDetail = await categoriaModel.create({
            nombre, tipo,descripcion
        })
    
        res.status(200).send({'msg':'Creado con éxito'});
        
    }catch(e){
        console.log('error');
        httpError(res,e);
    }

}




const listar = async (req, res) => {
    
    try{
        let listCategoria= await categoriaModel.find({});

        if(listCategoria.length <= 0){
            res.status(400).send({'msg':'No se encontró datos'});
        }else{
            res.status(200).send({'msg':'Consulta ejecutada con éxito', 'data': listCategoria});
        }
    }catch(e){
        httpError(res,e);
    }

}



module.exports = {crearCategoria, listar}
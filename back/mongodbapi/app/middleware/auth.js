const checkAuth= (req,res,next)=>{
    const token = req.header('auth-token')
    if (!token) return res.status(401).json({ error: 'Acceso denegado' })
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        next() //TODO: Se procede a continuar con el endpoint.
    } catch (error) {
        res.status(400).json({error: 'token no es válido'})
    }
}

module.exports = checkAuth
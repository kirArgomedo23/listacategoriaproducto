const calcularFechaMinima = () =>{
    let fechaMinima = null;
    const fecha = new Date();
    let anioActual = fecha.getFullYear();
    const mesActual = fecha.getMonth() + 1;
    const hoyActual = fecha.getDay() + 1;

    anioMinimo = anioActual - 18;
    fechaMinima = `${hoyActual}-${mesActual}-${anioMinimo}`
    
    return fechaMinima; //día-mes-año.
}

module.exports = calcularFechaMinima;
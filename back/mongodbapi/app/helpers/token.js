
const jwt = require('jsonwebtoken');


const generarToken = (req,res) => {
    const {alias, contrasenia} = req.body
    const token = jwt.sign({
        alias,
        contrasenia
    }, process.env.TOKEN_SECRET)
    
    return token;
}

const verificarToken = (req,res, next) =>{ 

}

module.exports  = {generarToken, verificarToken};
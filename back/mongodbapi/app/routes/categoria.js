
const express = require('express');
const router = express.Router();
const {crearCategoria, listar} = require('../controllers/categoria');


router.post('/', crearCategoria )
router.get('/listar', listar )


module.exports = router;
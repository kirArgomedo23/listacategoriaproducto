
const express = require('express');
const router = express.Router();
const {crearProducto,listarProductos} = require('../controllers/producto');


router.post('/', crearProducto );
router.get('/listar', listarProductos )


module.exports = router;
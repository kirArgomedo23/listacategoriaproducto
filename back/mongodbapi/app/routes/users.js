const express = require('express');
const router = express.Router();
const {getItems,getItem,createItem,loginUsuario} = require('../controllers/users');


router.get('/', getItems)

router.get('/:_id', getItem)

router.post('/', createItem)

router.post('/login', loginUsuario)



module.exports = router;
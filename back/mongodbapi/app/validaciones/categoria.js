
const Joi = require('@hapi/joi');

const validarCrearCategoria = Joi.object({
    nombre: Joi.string().min(4).max(50).regex(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable nombre debe ser string`,
        'string.min': `La variable nombre debe contener al menos 4 caracteres`,
        'string.max': `La variable nombre debe contener como máx 50 caracteres`,
        'any.required': `La variable nombre es requerido`,
        'string.pattern.base':`La variable nombre solo permite caracteres alfabeticos`
    }),
    tipo: Joi.string().min(3).max(50).regex(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable tipo debe ser string`,
        'string.min': `La variable tipo debe contener al menos 3 caracteres`,
        'string.max': `La variable tipo debe contener como máx 50 caracteres`,
        'any.required': `La variable tipo es requerido`,
        'string.pattern.base':`La variable tipo solo permite caracteres alfabeticos`
    }),
    descripcion: Joi.string().regex(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable descripcion debe ser string`,
        'string.empty': `La variable descripcion debe ser enviado`,
        'any.required': `La variable descripcion es requerido`,
        'string.pattern.base':`La variable descripcion solo permite caracteres alfabeticos`
    })
})


module.exports  = validarCrearCategoria;



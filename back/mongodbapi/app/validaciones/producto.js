
const Joi = require('@hapi/joi');

const validarCrearProducto = Joi.object({
    nombre: Joi.string().min(3).max(50).pattern(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable nombre debe ser string`,
        'string.min': `La variable nombre debe contener al menos 3 caracteres`,
        'string.max': `La variable nombre debe contener como máx 50 caracteres`,
        'any.required': `La variable nombre es requerido`,
        'string.pattern.base':`La variable nombre solo permite caracteres alfabeticos`
    }),
    descripcion: Joi.string().min(7).max(50).pattern(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable descripcion debe ser string`,
        'string.min': `La variable descripcion debe contener al menos 7 caracteres`,
        'string.max': `La variable descripcion debe contener como máx 50 caracteres`,
        'any.required': `La variable descripcion es requerido`,
        'string.pattern.base':`La variable descripcion solo permite caracteres alfabeticos`
    }),
    precioUnitario: Joi.number().required(),
    cantidad: Joi.number().required(),
    categoria: Joi.string().min(4).max(50).pattern(/^[a-z A-Z]+$/).required().messages({
        'string.base': `La variable categoria debe ser string`,
        'string.min': `La variable categoria debe contener al menos 4 caracteres`,
        'string.max': `La variable categoria debe contener como máx 50 caracteres`,
        'any.required': `La variable categoria es requerido`,
        'string.pattern.base':`La variable categoria solo permite caracteres alfabeticos`
    })
})


module.exports  = validarCrearProducto;
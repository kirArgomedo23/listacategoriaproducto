
const Joi = require('@hapi/joi');
const calcularFechaMinima = require('../helpers/general');

const validarRegistroUsuario = Joi.object({
    nombre: Joi.string().min(6).max(70).required(),
    apellidoPaterno: Joi.string().min(6).max(40).required(),
    apellidoMaterno: Joi.string().min(6).max(40).required(),
    fechaNacimiento: Joi.date().max(calcularFechaMinima()),
    alias: Joi.string().min(12).max(50).required(),
    email: Joi.string().min(12).required().email(),
    contrasenia: Joi.string().required().messages({
        'string.base': `La variable contrasenia debe ser númerica`,
        'string.empty': `La variable contrasenia debe ser enviado`,
        'any.required': `La variable contrasenia es requerido`
    })
})

const validarLoginUsuario = Joi.object({
    alias: Joi.string().min(12).max(50).required(),
    contrasenia: Joi.string().required().messages({
        'string.base': `La variable contrasenia debe ser númerica`,
        'string.empty': `La variable contrasenia debe ser enviado`,
        'any.required': `La variable contrasenia es requerido`
    })
})

module.exports  = {validarRegistroUsuario, validarLoginUsuario};




const mongoose = require('mongoose');
const categoria = mongoose.model('categoria');
const Schema = mongoose.Schema;

const ProductoSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    descripcion:{
        type:String,
        required:true
    },
    precioUnitario:{
        type:Number,
        required:true
    },
    cantidad:{
        type:Number,
        required:true
    },
    categoria: { 
        type:Schema.ObjectId, 
        ref: "categoria" 
    }
}, {
    timestamps:true,
    versionKey:false
});

module.exports = mongoose.model('producto', ProductoSchema);
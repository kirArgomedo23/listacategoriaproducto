const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    apellidoPaterno:{
        type:String,
        required:true
    },
    apellidoMaterno:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    fechaNacimiento: {
        type:Date,
        required:true
    },
    alias:{
        type:String,
        required:true
    },
    contrasenia:{
        type:String,
        required:true
    }
}, {
    timestamps:true,
    versionKey:false
});

module.exports = mongoose.model('user', UserSchema);
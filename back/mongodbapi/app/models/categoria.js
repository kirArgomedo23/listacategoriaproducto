
const mongoose = require('mongoose');

const CategoriaSchema = mongoose.Schema({
    nombre:{
        type:String,
        required:true
    },
    tipo:{
        type:String,
        required:true
    },
    descripcion:{
        type:String,
        required:true
    }
}, {
    timestamps:true,
    versionKey:false
});

module.exports = mongoose.model('categoria', CategoriaSchema);
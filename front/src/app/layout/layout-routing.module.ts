import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'categoria', pathMatch: 'prefix' },
      {path:'categoria', loadChildren: () => import('./categoria/categoria.module').then(m => m.CategoriaModule) },
      {path:'producto', loadChildren: () => import('./producto/producto.module').then(m => m.ProductoModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

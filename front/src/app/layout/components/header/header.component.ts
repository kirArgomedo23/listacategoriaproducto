import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() onclick: string;
  _opened: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  _toggleSidebar() {
    this._opened = !this._opened;
    console.log('dsa');
  }

}

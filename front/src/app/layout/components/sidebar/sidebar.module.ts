import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SidebarComponent} from './sidebar.component';
import { SidebarModule } from 'ng-sidebar';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    CommonModule, SidebarModule.forRoot()
  ],
  exports: [SidebarComponent]
})
export class SidebarModuleX { }

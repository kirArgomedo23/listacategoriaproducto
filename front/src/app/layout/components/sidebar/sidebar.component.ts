import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

   _opened: boolean = false;
   routes: any = [];
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.routes = this.createRoutes();
    console.log(this.routes )
  }

 
  _toggleSidebar() {
    this._opened = !this._opened;
  }

  goToNavigate(url){
    this.router.navigateByUrl(url);
  }

  createRoutes() {
    return [
      {
        link: '/especialista/profile',
        icon: 'fa fa-user-circle-o',
        title: 'Mi Cuenta'
      },
      {
        link: '/producto',
        icon: 'fa fa-fw fa-id-card-o',
        title: 'Producto',
      },
      {
        link: '/categoria',
        icon: 'fa fa-fw fa-id-badge',
        title: 'Categoria'
      }
    ];
}


}

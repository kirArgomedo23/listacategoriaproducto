import { Component, OnInit } from '@angular/core';
import {CategoriaService} from './categoria.service';
@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  DATOS: any = [];

  constructor(
    private categoriaService: CategoriaService
  ) { }

  ngOnInit(): void {
    this.listarCategorias();
  }

  listarCategorias(){
    this.categoriaService.listar().subscribe(data => {
      console.log('data', data.data);
      this.DATOS = data.data;
    }, error => {
      console.log('error');
    });
  }

}


import {CategoriaModel} from '../categoria/categoria-model';

export class ProductoModel {
    nombre: string;
    description: string;
    precioUnitario: number;
    cantidad: number;
    categoria: CategoriaModel
}

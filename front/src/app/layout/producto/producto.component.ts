import { Component, OnInit } from '@angular/core';
import {ProductoService} from './producto.service';
import {ProductoModel} from './producto-model';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  DATOS: any = [];

  constructor(
    private productoService: ProductoService
  ) { }

  ngOnInit(): void {
    this.listarProductos();
  }
  
  listarProductos(){
    this.productoService.listar().subscribe(data => {
      console.log('data', data.data);
      this.DATOS = data.data;
    }, error => {
      console.log('error');
    });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) { }

  listar(){
    const url = environment.domain_url +  'producto/listar';
    return this.http.get<any>(url).pipe(retry(2));
  }

  crear(){
    let datos;
    const url = environment.domain_url +  '/producto';

    return this.http.post<any>(url,datos).pipe(retry(2));
  }



}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry } from 'rxjs/operators';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  iniciarSesion(alias,contrasenia){
    const url = environment.domain_url +'users/login';
    
    let datos = {
      alias,
      contrasenia
    }
    return this.http.post<any>(url, datos).pipe(
      retry(2)
    );
  }

  crearUsuario(){
    const url = environment.domain_url +'users'
    
    return this.http.get<any>(url).pipe(
      retry(2)
    );
  }

}

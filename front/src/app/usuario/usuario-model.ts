export class UsuarioModel {
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    alias: string;
    email: string;
    contrasenia: string;
}

import { Component, OnInit } from '@angular/core';
import {UsuarioService} from '../usuario/usuario.service';
import {UsuarioModel} from '../usuario/usuario-model';
import { Router } from '@angular/router';


type LoginUsuario = Pick<UsuarioModel, "alias" | "contrasenia">;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  ERROR_LOGIN :boolean = false;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {

  }

  login(alias, contrasenia){
    const logueo: LoginUsuario = {
      alias,
      contrasenia
    };
    
    this.usuarioService.iniciarSesion(logueo.alias, logueo.contrasenia).subscribe(data=>{
      localStorage.setItem('token', data.token);
      this.router.navigateByUrl('/categoria');
    }, error=> {
      this.ERROR_LOGIN = true;
      
      setTimeout(()=> {
        this.ERROR_LOGIN = false;
      },2000);
    });
    
  }

}
